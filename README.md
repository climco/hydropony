# HydroPony

That is the repository for 3D printed assembly for [filka.io](filka.io)! To ensure scale you want, the calculator gonna be available on filka.io main site soon.
It is possible to use PVC hydraulic pipe instead of the 3D printed parts as a main core.



Thats note for writing scale calculator:
Dimension of 1,2,3 way pots roots site - 55mm
# CREDITS

We used some community parts as they were CC BY licensed.

https://www.thingiverse.com/thing:790339 - parametric netpot

Whole project is inspirated by [Alex Rodriquez modular hydroponics](https://www.thingiverse.com/thing:2403922)

STLs in CAD folder are taken from [Daniel Laszlo](https://www.thingiverse.com/boundarycondition/designs) and they are shared on [CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/) licence.

We decided to use his works, as it's making our job faster. To ensure calculations, dimension calculator gonna be available on filka.io main website soon. 


# TODO:

- finish the middle module, to be calculated from 3D printer dimension.
- modify [reprapable waterspray](https://www.thingiverse.com/thing:888404) to be used in this project
