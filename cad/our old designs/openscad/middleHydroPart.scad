 
//!OpenSCAD

mainRadius = 100;
mainThickness = 5;
potRadius = 30;
potThickness = 2;

// main block
difference() {
  cylinder(r1=mainRadius, r2=mainRadius, h=mainRadius, center=true);

  // main extrusion'
  cylinder(r1=(mainRadius - mainThickness), r2=(mainRadius - mainThickness), h=mainRadius, center=true);
  // extrusion of 1st pot place on X axis
  // 2nd pot place mirrored YZ
  translate([(mainRadius / 2), 0, (mainRadius / -2)]){
    rotate([0, 45, 0]){
      cylinder(r1=potRadius, r2=potRadius, h=mainRadius, center=false);
    }
  }
  // extrusion of 2nd pot place mirrored YZ
  rotate([0, 0, 60]){
    mirror([1,0,0]){
      // 2nd pot place mirrored YZ
      translate([(mainRadius / 2), 0, (mainRadius / -2)]){
        rotate([0, 45, 0]){
          // 2nd pot place mirrored YZ
          cylinder(r1=potRadius, r2=potRadius, h=mainRadius, center=false);
        }
      }
    }
  }
  // extrusion 3rd pot place mirrored YZ
  mirror([0,1,0]){
    rotate([0, 0, 60]){
      mirror([1,0,0]){
        // 2nd pot place mirrored YZ
        translate([(mainRadius / 2), 0, (mainRadius / -2)]){
          rotate([0, 45, 0]){
            // 2nd pot place mirrored YZ
            cylinder(r1=potRadius, r2=potRadius, h=mainRadius, center=false);
          }
        }
      }
    }
  }
}
//mainRadius / 2;

// 2nd pot place mirrored YZ
rotate([0, 0, 60]){
  mirror([1,0,0]){
    difference() {
      // 2nd pot place mirrored YZ2nd pot place mirrored YZ2nd pot place mirrored YZ2nd pot place mirrored YZ
      // 2nd pot place mirrored YZ
      translate([(mainRadius / 2), 0, (mainRadius / -2)]){
        rotate([0, 45, 0]){
          difference() {
            // 2nd pot place mirrored YZ
            cylinder(r1=potRadius, r2=potRadius, h=mainRadius, center=false);

            // 2nd pot place mirrored YZ
            // 2nd pot place mirrored YZ2nd pot place mirrored YZ2nd pot place mirrored YZ
            // 2nd pot place mirrored YZ2nd pot place mirrored YZ2nd pot place mirrored YZ
            cylinder(r1=(potRadius - potThickness), r2=(potRadius - potThickness), h=mainRadius, center=false);
          }
        }
      }

      // 2nd pot place mirrored YZ
      translate([0, 0, (mainRadius / -2)]){
        // main extrusion'
        cylinder(r1=(mainRadius - mainThickness), r2=(mainRadius - mainThickness), h=(mainRadius * 2), center=true);
      }
    }
  }
}
// 3rd pot place mirrored XZ
mirror([0,1,0]){
  rotate([0, 0, 60]){
    mirror([1,0,0]){
      difference() {
        // 2nd pot place mirrored YZ2nd pot place mirrored YZ2nd pot place mirrored YZ2nd pot place mirrored YZ
        // 2nd pot place mirrored YZ
        translate([(mainRadius / 2), 0, (mainRadius / -2)]){
          rotate([0, 45, 0]){
            difference() {
              // 2nd pot place mirrored YZ
              cylinder(r1=potRadius, r2=potRadius, h=mainRadius, center=false);

              // 2nd pot place mirrored YZ
              // 2nd pot place mirrored YZ2nd pot place mirrored YZ2nd pot place mirrored YZ
              // 2nd pot place mirrored YZ2nd pot place mirrored YZ2nd pot place mirrored YZ
              cylinder(r1=(potRadius - potThickness), r2=(potRadius - potThickness), h=mainRadius, center=false);
            }
          }
        }

        // 2nd pot place mirrored YZ
        translate([0, 0, (mainRadius / -2)]){
          // main extrusion'
          cylinder(r1=(mainRadius - mainThickness), r2=(mainRadius - mainThickness), h=(mainRadius * 2), center=true);
        }
      }
    }
  }
}
// 1st pot place on X axis
difference() {
  // 2nd pot place mirrored YZ2nd pot place mirrored YZ2nd pot place mirrored YZ
  translate([(mainRadius / 2), 0, (mainRadius / -2)]){
    rotate([0, 45, 0]){
      difference() {
        // 2nd pot place mirrored YZ
        cylinder(r1=potRadius, r2=potRadius, h=mainRadius, center=false);

        // 2nd pot place mirrored YZ
        // 2nd pot place mirrored YZ2nd pot place mirrored YZ2nd pot place mirrored YZ
        // 2nd pot place mirrored YZ2nd pot place mirrored YZ2nd pot place mirrored YZ
        cylinder(r1=(potRadius - potThickness), r2=(potRadius - potThickness), h=mainRadius, center=false);
      }
    }
  }

  translate([0, 0, (mainRadius / -2)]){
    // main extrusion'
    cylinder(r1=(mainRadius - mainThickness), r2=(mainRadius - mainThickness), h=(mainRadius * 2), center=true);
  }
}
